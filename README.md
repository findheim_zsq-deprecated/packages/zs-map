# zs-map

This is a library of a map using mapbox

## Dependencies

### Environment variables
currently there are no needed env vars.

## Development

### Setup
`npm install` is the only needed command for setup and will also start `bower install`

### Running
`gulp` - will run a development build and then start the dev environment server with browser-sync (all changes to code will take effect immediately)

### Building
`gulp build` - will start a production build and generate `build/script.js` and `build/style.js`  
these 2 files need to be commited and are the only thing that will be used by anyone including this lib

### Scripts
Code should be in coffeescript inside the folder `src/scripts` (or a subfolder).  
All files there will be compiled and concatinated to a single file.

### Libraries
Javascript libraries can be put in the folder `src/lib` (or a subfolder) or referenced in the file `src/lib/external.json`.  
external.json has to be an array of string, where each string is the path to a single js file (e.g: `["./bower_components/library/index.js"]`).  
All js files will be concatinated and prepended in the scripts file.

### Styles
There is one main file for scss at `src/lib/style/screen.scss`, additional scss files have to be imported in this file.  
css files can be put in `src/lib/style/` (or a subfolder) or referenced in the file `src/style/external.json`.  
external.json has to be an array of string, where each string is the path to a single js file (e.g: `["./bower_components/library/index.css"]`).  
All Style files will be compiled and concatinated to a single file.

### Dev environment
The folder `dev` holds all files only needed for the dev environment.  
There is an express server (startet with gulp) to run the environment, and the index.html, which will load js and css and includes the demo map.

## Interface
### window.zsMap
- **create(id, options)**: creates a new map in a given container  
    - id: the DOM id of a div container, which should hold the map
    - options:  
        - country: at / de - the country, for which to show the map (will set center, maxBounds and topology to show) [default: at]
        - language: de / en - the language to display text in [default: de]
        - bbox: the bbox, which the map should show on load
        - topo:
            - display: true / false - display the topology and entity labels on hover [default: true]
            - search: true / false - allow search actions, will also display controls to switch mapSearch and clickable [default: false]
            - mapSearch: true / false - use map search if true, location search if false [default: false]
            - clickable: true / false - only available for location search; if true, current selection is shown on map and entities can be selected with click [default: false]
            - entities: array of ids of currently selected topo entities
    - RETURN: zsMap
- **setConfig(options)**: change config options
    - allowed options are all from create except country, bbox, topo.entities
- **on(event, tag, function)**: registers a function for an event
    - event: name of the event
    - tag: tag for event (to remove it later) [optional]
    - function: function register, will be called with 1 parameter *payload* (depending on eventType)
- **off(tag)**: remove all event listeners with given tag
    - tag: tag of events to remove
    - RETURN: number of removed event listeners
- **setEntities(ids)**: set and overried currently selected topo entities
    - ids: array of ids of currently selected topo entities
- **getEntities()**: get all currently selected entities
    - RETURN: array of ids of currently selected entities
- **addEntity(id)**: add a single topo entity
    - id: id of the entity to add
- **removeEntity(id)**: remove a single topo entity
    - id: id of the entity to remove
- **showGeo(geo)**: displays a given geo on the map (only one can be shown, calling it again will override first geo)
    - geo: the geoJSON to display
    - RETURN: true if it worked, else false
- **hideGeo()**: hide currently displayed geo
- **showMarkers(geos)**: show markers in center of each given geo, old markers will be hidden
    - geos: Object - keys: id, values: geoJson; null to only hide markers
    - RETURN: true if it worked else false
- **panTo(bbox, options)**: moves the map to the given bbox
    - bbox: the bbox to move the map to
    - options:
        - smooth: Boolean; if true, the map will fly to new location, otherwise it will jump directly there [default: false]
- **resize()**: redraws the map, must be called after size change of container
- **getBBox()**: get the current bbox
    - RETURN: current bbox as array

### Events
- **load**: fired once the map has loaded [no payload]
- **move**: map bbox has changed (move, zoom) [payload: new bbox]
- **clickOnEntity**: a topo entity has been clicked [payload: {id: entityId}]
- **switchMapSearch**: map search has been switched on or off [payload: Boolean (new status of mapSearch)]