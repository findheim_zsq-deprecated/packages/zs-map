class ZsMapUtils
  _mergeBbox: (bbox1, bbox2)->
    bbox1[0][0] = Math.min(bbox1[0][0], bbox2[0][0])
    bbox1[0][1] = Math.min(bbox1[0][1], bbox2[0][1])
    bbox1[1][0] = Math.max(bbox1[1][0], bbox2[1][0])
    bbox1[1][1] = Math.max(bbox1[1][1], bbox2[1][1])
    return bbox1
  
  _getBboxOfCoords: (coords)->
    if typeof coords[0] == 'number'
      return [coords,coords]
    bbox = [[180,90],[0,0]]
    for c in coords
      bbox = @_mergeBbox bbox, @_getBboxOfCoords(c)
    return bbox
  
  getBbox: (geo)->
    switch geo.type
      when 'GeometryCollection'
        bbox = [[180,90],[0,0]]
        for geometry in geo.geometries
          bbox = @_mergeBbox bbox, @getBbox(geometry)
        return bbox
      when 'FeatureCollection'
        bbox = [[180,90],[0,0]]
        for feature in geo.features
          bbox = @_mergeBbox bbox, @getBbox(feature)
        return bbox
      when 'Feature'
        return @getBbox geo.geometry
      else
        return @_getBboxOfCoords geo.coordinates

  getCenter: (geo)->
    bbox = @getBbox geo
    return [(bbox[0][0]+bbox[1][0]) / 2, (bbox[0][1]+bbox[1][1]) / 2]
    
  setClass: (elem, className, enabled)->
    return unless elem?
    if enabled
      unless new RegExp("(?:^|\\s)#{className}(?!\\S)").test(elem.className)
        elem.className += " #{className}"
    else
      regex = new RegExp("(?:^|\\s)#{className}(?!\\S)","g")
      elem.className = elem.className.replace(regex, '')
    return
    
  formatValue: (val)->
    if val?
      return Math.round(val).toString().replace(/\B(?=(\d{3})+(?!\d))/g,".")
    else
      return ""