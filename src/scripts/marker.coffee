class ZsMapMarker
  init: (ctrl)->
    @ctrl = ctrl
    ctrl._map.addSource 'zs-marker-src',
      type: 'geojson'
      data: {"type":"FeatureCollection","features":[]}
      cluster: true
      clusterMaxZoom: ctrl._config.style.marker.clusterMaxZoom
      clusterRadius: ctrl._config.style.marker.clusterRadius
    ctrl._map.addSource 'zs-marker-geo-src',
      type: 'geojson'
      data: {"type":"FeatureCollection","features":[]}
    ctrl._map.addSource 'zs-marker-active-src',
      type: 'geojson'
      data: {"type":"FeatureCollection","features":[]}

    ctrl._map.addLayer
      id: 'zs-marker-geo-bg'
      type: 'fill'
      source: 'zs-marker-geo-src'
      paint:
        'fill-color': ctrl._config.style.geo.background
    ctrl._map.addLayer
      id: 'zs-marker-geo-border'
      type: 'line'
      source: 'zs-marker-geo-src'
      paint:
        'line-color': ctrl._config.style.geo.color
        'line-width': ctrl._config.style.geo.width
    ctrl._map.addLayer
      id: 'zs-marker'
      type: 'symbol'
      source: 'zs-marker-src'
      layout:
        'icon-image': 'zs-marker'
        'icon-allow-overlap': true
      filter: ['!=', 'cluster', true]
    layers = ctrl._config.style.marker.layers
    layers.forEach (layer, i)->
      ctrl._map.addLayer
        id: "zs-marker-" + i + "-border"
        type: "circle"
        source: "zs-marker-src"
        paint:
          'circle-color': '#006B94'
          'circle-radius': layer.r + 1
        filter:
          if i == 0
            ['<', 'point_count', layers[i + 1].nr]
          else if i == ( layers.length - 1 )
            ['>=', 'point_count', layer.nr]
          else
            ['all',['>=', 'point_count', layer.nr],['<', 'point_count', layers[i + 1].nr]]
      ctrl._map.addLayer
        id: "zs-marker-" + i
        type: "circle"
        source: "zs-marker-src"
        paint:
          'circle-color': '#8cd9ea'
          'circle-radius': layer.r
        filter: 
          if i == 0
            ['<', 'point_count', layers[i + 1].nr]
          else if i == ( layers.length - 1 )
            ['>=', 'point_count', layer.nr]
          else
            ['all',['>=', 'point_count', layer.nr],['<', 'point_count', layers[i + 1].nr]]
    ctrl._map.addLayer
      id: "zs-marker-count"
      type: "symbol"
      source: "zs-marker-src"
      layout:
        "text-field": "{point_count}"
        "text-font": ["Roboto Regular"]
        "text-size": 14
        "text-offset": [0,0.1]
      filter: ['==', 'cluster', true]
    ctrl._map.addLayer
      id: 'zs-marker-active'
      type: 'symbol'
      source: 'zs-marker-active-src'
      layout:
        'icon-image': 'zs-marker-active'
    
    ctrl._map.on 'mousemove', (e)=>
      if ctrl._config.topo.search
        features = ctrl._map.queryRenderedFeatures e.point, {layers: ["zs-marker", "zs-marker-0", "zs-marker-1", "zs-marker-2", "zs-marker-count"]}
        if features.length > 0
          document.querySelector('.mapboxgl-canvas-container').style.cursor = 'pointer'
          unless features[0].properties.cluster
            @showGeo JSON.parse(features[0].properties.geo), JSON.parse(features[0].properties.center)
        else
          document.querySelector('.mapboxgl-canvas-container').style.cursor = ''
          @showGeo()

    ctrl._map.on "mouseout", (e)=>
      if ctrl._config.topo.search
        if e.point.x < 0 || e.point.y < 0 || e.point.x >= ctrl._mapContainer.clientWidth || e.point.y >= ctrl._mapContainer.clientHeight
          document.querySelector('.mapboxgl-canvas-container').style.cursor = ''
          @showGeo()

    elem = document.createElement('div')
    elem.className = 'hidden zsmap-marker-container-estateList'
    @_markerEstateList = new mapboxgl.Marker(elem).setLngLat(ctrl._config[ctrl._config.country].center).addTo(ctrl._map)
    ctrl._map.on 'click', (e)=>
      features = ctrl._map.queryRenderedFeatures e.point, {layers: ["zs-marker", "zs-marker-0", "zs-marker-1", "zs-marker-2", "zs-marker-count"]}
      if features.length > 0
        if features[0].properties.cluster
          @_hideMarkerEstate()
          ctrl._map.zoomTo(ctrl._map.getZoom() + 2,{around: e.lngLat})
        else
          @_markerEstateList.setLngLat e.lngLat
          elem = @_markerEstateList.getElement()
          elem.innerHTML = ''
          child = document.createElement('div')
          child.className = "zsmap-marker-estateList"
          translateX = 10
          translateY = 10
          if (e.point.x + 240) > @ctrl._mapContainer.clientWidth
            translateX = if e.point.x >= 240 then -240 else -115
          if (e.point.y + 170) > @ctrl._mapContainer.clientHeight
            translateY = if e.point.y >= 170 then -170 else -80
          child.style.transform = "translate(#{translateX}px,#{translateY}px)"
          child.innerHTML = "<div class='zsmap-marker-estateList__img' style='background-image:url(#{features[0].properties.img})'></div>#{features[0].properties.type}<br> <strong>#{features[0].properties.text}</strong><br><a class='button button--mini button--positive'>Details</a>"
          child.addEventListener 'click', (e)->
            e.stopPropagation()
            ctrl._trigger 'estateDetail', features[0].properties.id
          child.addEventListener 'mousedown', (e)-> e.stopPropagation()
          child.addEventListener 'mousemove', (e)-> e.stopPropagation()
          child.addEventListener 'mouseup', (e)-> e.stopPropagation()
          closeButton = document.createElement('a')
          closeButton.className = "zsmap-marker-estateList__close zsicon2-close"
          closeButton.addEventListener 'click', (e)=>
            e.stopPropagation()
            @_hideMarkerEstate()
          child.appendChild(closeButton)
          elem.appendChild child
          elem.className = elem.className.replace(/(?:^|\s)hidden(?!\S)/g , '')
      else
        @_hideMarkerEstate()

    ctrl._map.on 'zoom', ()=>
      @_hideMarkerEstate()

  _hideMarkerEstate: ()->
    unless /(?:^|\s)hidden(?!\S)/.test(@_markerEstateList.getElement().className)
      @_markerEstateList.getElement().className += ' hidden'

  _centerOffsets: [[0.00005,0],[0,-0.00005],[-0.00005,0],[0,0.00005],[0.00005,-0.00005],[-0.00005,-0.00005],[-0.00005,0.00005],[0.00005,0.00005]]
  showMarkers: (estates)->
    @_hideMarkerEstate()
    data = {type: "FeatureCollection", features:[]}
    elems = {}
    for estate in estates or []
      center = @ctrl.utils.getCenter(estate.geo.shape)
      if elems[center.toString()]?
        found = false
        for offset in @_centerOffsets
          newCenter = [center[0] + offset[0],center[1] + offset[1]]
          unless elems[newCenter.toString()]?
            center = newCenter
            found = true
            break
        unless found
          center = null
      if center?
        elems[center.toString()] =
          center: center
          data:
            center: JSON.stringify(center)
            geo: JSON.stringify(estate.geo.shape)
            id: estate.extId
            type: @ctrl._config.translations[@ctrl._config.language].types[estate.types.view] or estate.types.view
            text: "#{@ctrl.utils.formatValue(estate.data?.space?.value)} m² / #{if estate.data?.rooms? then (estate.data?.rooms + " " + @ctrl._config.translations[@ctrl._config.language].rooms + " / ") else ""}#{@ctrl.utils.formatValue(estate.data?.price?.value)} €"
            img: estate.media?.images?[0] or ""
    for i, elem of elems
      data.features.push {type: "Feature", geometry: {type: "Point", coordinates: elem.center}, properties: elem.data}
    @ctrl._map.getSource('zs-marker-src').setData(data)
    return true
    
  showGeo: (geo, center)->
    if geo?
      @ctrl._map.getSource('zs-marker-geo-src').setData({"type":"FeatureCollection","features":[{type: "Feature", geometry: geo}]})
      @ctrl._map.getSource('zs-marker-active-src').setData({"type":"FeatureCollection","features":[{type: "Feature", geometry: {type: "Point", coordinates: center or @ctrl.utils.getCenter(geo)}}]})
    else
      @ctrl._map.getSource('zs-marker-geo-src').setData({"type":"FeatureCollection","features":[]})
      @ctrl._map.getSource('zs-marker-active-src').setData({"type":"FeatureCollection","features":[]})