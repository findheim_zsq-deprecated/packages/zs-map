class ZsMapFallback
  use: (ctrl, options)->
    @id = ctrl._mapId
    @bbox = options.bbox
    head = document.getElementsByTagName("head")[0]
    style = document.createElement('link')
    style.href = '/assets/shared/css/leaflet.css'
    style.rel = 'stylesheet'
    style.type = 'text/css'
    head.insertBefore(style, head.firstChild)
    script = document.createElement('script')
    script.type = 'text/javascript'
    script.src = '/assets/shared/js/leaflet.js'
    head.appendChild(script)
    return this 