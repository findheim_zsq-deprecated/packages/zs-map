class ZsMapConfig
  language: 'de'
  country: 'at'
  at:
    center: [16.37, 48.2]
    maxBounds: [[9.23, 46.07],[17.46, 49.32]]
    topoSource: 'mapbox://zoomsquare.2of0w75z'
    tileLayer: "zs-topo-at-b6x9gf"
  de:
    center: [13.4, 52.52]
    maxBounds: [[5.56, 46.97],[15.34, 55.21]]
    topoSource: 'mapbox://zoomsquare.2nn1cr16'
    tileLayer: "zs-topo-de-b6jign"
  translations:
    de:
      styleMap: 'Karte'
      styleAerial: 'Luftbild'
      modeSwitch: '<i class="zsicon2-cursor-pointer"></i>: Wähle Orte durch Klicken auf der Karte aus<br><i class="zsicon2-map-marker-search"></i>: Suche im Kartenausschnitt'
      types: "###zsTranslationTypesDe###"
      rooms: "Zi"
    en:
      styleMap: 'Map'
      styleAerial: 'Aerial map'
      modeSwitch: '<i class="zsicon2-cursor-pointer"></i>: Choose locations by clicking on the map<br><i class="zsicon2-map-marker-search"></i>: Search in map section'
      types: "###zsTranslationTypesEn###"
      rooms: "rooms"
  topo:
    display: true #display topology
    selection: false # only for search=false, show selected entitites
    search: false #allow all search actions
    mapSearch: false # only for search=true, true -> map search, false -> location search
    estateMode: false
  style:
    tiles:
      default: 'mapbox://styles/zoomsquare/ciqs65i7y000566m19n9w4ft8'
      sat: 'mapbox://styles/zoomsquare/ciqwcdx1y000ggsmjmpm2hmiu'
    topo:
      color: 'rgba(150,150,150,0.8)'
      width: 1
      hover: 'rgba(40,51,55,0.15)'
      selected: 'rgba(40,51,55,0.3)'
    geo:
      color: 'rgba(0,163,224,1)'
      width: 2
      background: 'rgba(0,163,224,0.6)'
    marker:
      clusterMaxZoom: 15
      clusterRadius: 40
      layers: [
        {nr: 0, r: 16}
        {nr: 5, r: 19}
        {nr: 10, r: 22}
      ]
      
  setOptions: (ctrl, options, init)->
    changes = 
      topo: {}
    if options.language in ['de','en'] and options.language != @language
      @language = changes.language = options.language
    #topo
    if options.topo?.estateMode? and @topo.estateMode != options.topo.estateMode
      @topo.estateMode = changes.topo.estateMode = options.topo.estateMode
    if @topo.estateMode
      if @topo.search
        @topo.search = changes.topo.search = false
    else
      if options.topo?.search? and @topo.search != options.topo.search
        @topo.search = changes.topo.search = options.topo.search
      if @topo.search
        if options.topo?.mapSearch? and @topo.mapSearch != options.topo.mapSearch
          @topo.mapSearch = changes.topo.mapSearch = options.topo.mapSearch
      else
        if @topo.mapSearch
          @topo.mapSearch = changes.topo.mapSearch = false
      if options.topo?.selection? && @topo.selection != options.topo.selection
        @topo.selection = changes.topo.selection = options.topo.selection

    unless init
      if changes.language?
        ctrl._controlHelper.updateLanguage()
      if changes.topo?.search?
        ctrl._controlHelper.enableModeControl changes.topo.search
        console.log "set layers: ", @topo.search
        for layer in ["zs-marker", "zs-marker-0", "zs-marker-1", "zs-marker-2", "zs-marker-0-border", "zs-marker-1-border", "zs-marker-2-border", "zs-marker-count"]
          ctrl._map.setLayoutProperty layer, 'visibility', if @topo.search then 'visible' else 'none'
      if changes.topo?.mapSearch?
        ctrl._controlHelper.setMapMode()
        if @topo.search
          ctrl._trigger 'switchMapSearch', changes.topo.mapSearch
      if changes.topo?.search? || changes.topo?.mapSearch? || changes.topo?.selection?
        if (@topo.search && !@topo.mapSearch) || (!@topo.search && @topo.selection)
          ctrl._map.setLayoutProperty('zs-topo-selected', 'visibility', 'visible')
        else
          ctrl._map.setLayoutProperty('zs-topo-selected', 'visibility', 'none')