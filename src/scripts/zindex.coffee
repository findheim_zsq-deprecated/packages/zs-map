class ZsMap
  _loaded: false
  _selectedEntities: {}
  _controlHelper: new ZsMapControlHelper()
  _config: new ZsMapConfig()
  _marker: new ZsMapMarker() 
  utils: new ZsMapUtils()
  _afterLoadFns: []
  
  create: (mapId, options)->
    @_mapId = mapId
    @_mapContainer = document.querySelector("##{@_mapId}")
    mapboxgl.accessToken = 'pk.eyJ1Ijoiem9vbXNxdWFyZSIsImEiOiJiYTFhYmZlODkwNzQwZTc1OGQxMThjMTU1NzU1ZmM2ZSJ9.2eXIKouulITlZRWPCQRgkA'
    
    options = options or {}
    if options.country in ['at','de']
      @_config.country = options.country
    if options.language in ['de','en']
      @_config.language = options.language
    if options.topo?.entities?
      for entity in options.topo.entities
        @_selectedEntities[entity] = true
    @_config.setOptions @, options, true

    if options.bbox?
      camera = @_convertBboxToCoords(options.bbox)
      zoom = camera.zoom
      center = camera.center
    else
      zoom = 10
      center = @_config[@_config.country].center

    if mapboxgl.supported()
      @_map = new mapboxgl.Map
        container: mapId
        style: @_config.style.tiles.default
        zoom: zoom
        minZoom: 7
        maxZoom: 18
        center: center
        maxBounds: @_config[@_config.country].maxBounds
        attributionControl: false
  
      @_controlHelper.init(this)
  
      @_map.on 'load', ()=>
        @_loaded = true
        @_addTopo()
        @_marker.init @
        
        hideHover = ()=>
          @currentHover = null
          @_map.setFilter "zs-topo-hover", ["==", "i", ""]
          unless /(?:^|\s)hidden(?!\S)/.test(@_topoNameMarker._element.className)
            @_topoNameMarker._element.className += ' hidden'
            
        @_map.on 'zoom', ()=>
          hideHover()
          filter = @_getTopoFilter()
          @_map.setFilter 'zs-topo', filter
          @_map.setFilter 'zs-topo-border', filter
  
        elem = document.createElement('div')
        elem.className = 'hidden'
        elem.innerHTML = '<div class="zsmap-marker-toponame"></div>'
        @_topoNameMarker = new mapboxgl.Marker(elem).setLngLat(center).addTo(@_map)
        
        @_map.on 'mousemove', (e)=>
          if @_map.queryRenderedFeatures(e.point, {layers: ["zs-marker", "zs-marker-0", "zs-marker-1", "zs-marker-2", "zs-marker-count"]}).length == 0
            features = @_map.queryRenderedFeatures e.point, {layers: ["zs-topo"]}
            if features.length > 0
              @_topoNameMarker.setLngLat e.lngLat
              unless @currentHover == features[0].properties.i
                @currentHover = features[0].properties.i
                @_map.setFilter 'zs-topo-hover', ["==", "i", features[0].properties.i]
                @_topoNameMarker._element.children[0].innerHTML = features[0].properties.n 
                @_topoNameMarker._element.className = @_topoNameMarker._element.className.replace(/(?:^|\s)hidden(?!\S)/g , '')
            else
              hideHover()
          else
            hideHover()
  
        @_map.on "mouseout", (e)=>
          if e.point.x < 0 || e.point.y < 0 || e.point.x >= @_mapContainer.clientWidth || e.point.y >= @_mapContainer.clientHeight
            hideHover()
          
        @_map.on 'click', (e)=>
          if @_config.topo.search && !@_config.topo.mapSearch && @_map.queryRenderedFeatures(e.point, {layers: ["zs-marker", "zs-marker-0", "zs-marker-1", "zs-marker-2", "zs-marker-count"]}).length == 0
            features = @_map.queryRenderedFeatures e.point, {layers: ["zs-topo-selected"]}
            if features.length > 0
              elem = {level: 0, id: ""}
              for f in features
                for i in [10,9,8,6,4]
                  if f.properties[i]
                    break
                if i > elem.level
                  elem = {level: i, id: f.properties.i}
              delete @_selectedEntities[elem.id]
              action = "remove"
            else
              features = @_map.queryRenderedFeatures e.point, {layers: ["zs-topo"]}
              if features.length > 0
                @_selectedEntities[features[0].properties.i] = true
                action = "add"
            if action?
              @_setSelectedFilter() 
              @_trigger 'clickOnEntity', 
                action: action
                id: features[0].properties.i
                name: features[0].properties.n
            
        @_map.on 'moveend', ()=>
          unless @estateMode
            @_trigger 'move', @_map.getBounds().toArray()
  
        #source, layers and marker for geo
        elem = document.createElement('div')
        elem.className = 'zsmap-marker-geo hidden'
        @_geoMarker = new mapboxgl.Marker(elem).setLngLat(center).addTo(@_map)
        @_map.addSource "zsGeo-src",
          type: 'geojson'
          data:
            type: 'Feature'
            geometry: null
        @_map.addLayer
          id: "zsGeo-bg"
          type: 'fill'
          source: "zsGeo-src"
          paint:
            'fill-color': @_config.style.geo.background
        @_map.addLayer
          id: "zsGeo-border"
          type: 'line'
          source: "zsGeo-src"
          paint:
            'line-color': @_config.style.geo.color
            'line-width': @_config.style.geo.width
        @_map.setLayoutProperty('zsGeo-bg', 'visibility', 'none')
        @_map.setLayoutProperty('zsGeo-border', 'visibility', 'none')
  
        @_map.addSource 'zs-bbox-src',
          type: 'geojson'
          data: {"type":"FeatureCollection","features":[]}
        @_map.addLayer
          id: 'zs-bbox'
          type: 'line'
          source: 'zs-bbox-src'
          paint:
            'line-color': "rgba(0,0,0,1)"
            'line-width': 2
            'line-blur': 1
        
        for elem in @_afterLoadFns
          @[elem.fn] elem.p1, elem.p2
        @_afterLoadFns = []
        @_trigger('load')
  
      return this
    else #no webgl support
      @fallback = new ZsMapFallback().use(@, {bbox: options.bbox})
    
  _addTopo: ()->
    filter = @_getTopoFilter()
    @_map.addSource 'zs-topology',
      type: 'vector',
      url: @_config[@_config.country].topoSource
    @_map.addLayer
      id: 'zs-topo'
      type: 'fill'
      source: 'zs-topology'
      'source-layer': @_config[@_config.country].tileLayer
      paint:
        "fill-opacity": 0
      "filter": filter
    @_map.addLayer
      id: 'zs-topo-border'
      type: 'line'
      source: 'zs-topology'
      'source-layer': @_config[@_config.country].tileLayer
      paint:
        "line-color": @_config.style.topo.color
        "line-width": @_config.style.topo.width
      "filter": filter
    @_map.addLayer
      id: 'zs-topo-hover'
      type: 'fill'
      source: 'zs-topology'
      'source-layer': @_config[@_config.country].tileLayer
      paint:
        "fill-color": @_config.style.topo.hover
      "filter": ['==','i','']
    @_map.addLayer
      id: 'zs-topo-selected'
      type: 'fill'
      source: 'zs-topology'
      'source-layer': @_config[@_config.country].tileLayer
      paint:
        "fill-color": @_config.style.topo.selected
      "filter": ['in','i','']
    if @_config.topo.mapSearch
      @_map.setLayoutProperty('zs-topo-selected', 'visibility', 'none')

    @_setSelectedFilter()
    
  setConfig: (options)->
    return unless options?
    unless @_loaded
      @_afterLoadFns.push {fn: 'setConfig', p1: options}
      return
    @_config.setOptions @, options
  
  showGeo: (geo)->
    unless @_loaded
      @_afterLoadFns.push {fn: 'showGeo', p1: geo}
      return
    @_marker.showGeo geo

  hideGeo: ()->
    unless @_loaded
      @_afterLoadFns.push {fn: 'hideGeo'}
      return
    @_marker.showGeo()
    
  showEstate: (geo)->
    unless @_loaded
      @_afterLoadFns.push {fn: 'showEstate', p1: geo}
      return
    if geo?
      unless @_config.topo.estateMode
        @estateModeValues =
          search: @_config.topo.search
          bbox: @_map.getBounds().toArray()
        @_config.setOptions @, {topo: {estateMode: true}}
      @showGeo geo  
      @panTo @utils.getBbox(geo),{smooth: true, padding: 1}    
    
  hideEstate: ()->
    unless @_loaded
      @_afterLoadFns.push {fn: 'hideEstate'}
      return
    if @_config.topo.estateMode
      @hideGeo()
      if @estateModeValues?
        @panTo @estateModeValues.bbox,{smooth: true, padding: 0}    
      @_config.setOptions @, {topo: {estateMode: false, search: @estateModeValues?.search}}
      @estateModeValues = null

  showMarkers: (estates)->
    unless @_loaded
      @_afterLoadFns.push {fn: 'showMarkers', p1: estates}
      return
    @_marker.showMarkers(estates)
  
  _events:
    'clickOnEntity': []
    'move': []
    'load': []
    'switchMapSearch': []
    'estateDetail': []
  _trigger: (event, payload)->
    if @_events[event]?
      for e in @_events[event]
        e.fn payload
    else
      throw new Error("zs-map._trigger: invalid event (#{event})")
  on: (event, tag, fn)->
    unless fn?
      fn = tag
      tag = null
    unless event?
      throw new Error("zs-map.on: 1st param missing (event)")
    unless fn?
      throw new Error("zs-map.on: 2nd param missing (function)")
    if @_events[event]?
      @_events[event].push
        tag: tag
        fn: fn
    else
      throw new Error("zs-map.on: invalid event (#{event})")
    return
  off: (tag)->
    unless tag?
      return 0
    count = 0
    for name, arr of @_events
      if arr.length > 0
        for index in [arr.length - 1..0]
          if arr[index].tag == tag
            count++
            arr.splice index, 1
    return count
      
  panTo: (bbox, options)->
    options = {} unless options?
    options.padding = 0.1 unless options.padding?
    unless @_loaded
      @_afterLoadFns.push {fn: 'panTo', p1: bbox, p2: options}
      return
    camera = @_convertBboxToCoords(bbox, options.padding)
    if options.smooth
      @_map.flyTo
        center: camera.center
        zoom: camera.zoom
        speed: options.speed or 2
    else
      @_map.jumpTo
        center: camera.center
        zoom: camera.zoom
        
  resize: ()->
    unless @_loaded
      @_afterLoadFns.push {fn: 'resize'}
      return
    @_map.resize()
    
  getBBox: ()->
    @_map.getBounds().toArray()
    
  getEntities: ()->
    Object.keys(@_selectedEntities)
    
  setEntities: (ids)->
    unless @_loaded
      @_afterLoadFns.push {fn: 'setEntities', p1: ids}
      return
    @_selectedEntities = {}
    for id in ids
      @_selectedEntities[id] = true
    @_setSelectedFilter()
    
  addEntity: (id)->
    unless @_loaded
      @_afterLoadFns.push {fn: 'addEntity', p1: id}
      return
    @_selectedEntities[id] = true
    @_setSelectedFilter()

  removeEntity: (id)->
    unless @_loaded
      @_afterLoadFns.push {fn: 'removeEntity', p1: id}
      return
    delete @_selectedEntities[id]
    @_setSelectedFilter()

  _convertBboxToCoords: (bbox, padding = 0)->
    widthLvl7 = 4.394531250000085 * @_mapContainer.clientWidth / 800
    heightLvl7 = 2.195395761319901 * @_mapContainer.clientHeight / 600
    widthBbox = bbox[1][0] - bbox[0][0]
    heigthBbox = bbox[1][1] - bbox[0][1]
    widthZoom = 7 + Math.log2(Math.abs(widthLvl7 / widthBbox))
    heightZoom = 7 + Math.log2(Math.abs(heightLvl7 / heigthBbox))
    return {
      center: [(bbox[0][0] + bbox[1][0]) / 2, (bbox[0][1] + bbox[1][1]) / 2]
      zoom: Math.min(Math.min(widthZoom, heightZoom) - padding, 16)
    }

  _getTopoFilter: ()->
    zoom = @_map.getZoom()
    if zoom > 12 then lvl = 10
    else if zoom > 9.8 then lvl = 9
    else if zoom > 9 then lvl = 8
    else if zoom > 7 then lvl = 6
    else lvl = 4
    return ['has',"#{lvl}"]
  
  _setSelectedFilter: ()->
    if Object.keys(@_selectedEntities).length > 0
      filter = ['in','i']
      for key of @_selectedEntities
        filter.push key
    else
      filter = ['in','i','']
    @_map.setFilter 'zs-topo-selected', filter
    
  remove: ()->
    for key of @_events
      @_events[key] = []
    @_map.remove()
    window.zsMap = new ZsMap()
    
  showBbox: (bbox)->
    if bbox?
      @_map.getSource('zs-bbox-src').setData({"type":"Polygon","coordinates":[[bbox[0],[bbox[0][0], bbox[1][1]],bbox[1],[bbox[1][0], bbox[0][1]],bbox[0]]]})
    else
      @_map.getSource('zs-bbox-src').setData({"type":"FeatureCollection","features":[]})

window.zsMap = new ZsMap()