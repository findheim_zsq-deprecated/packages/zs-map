class ZsMapControlHelper
  init: (ctrl)->
    @_modeControl = new ZsMapModeControl(ctrl)
    ctrl._map.addControl @_modeControl
    unless ctrl._config.topo.search
      @enableModeControl false
    @_navControl = new ZsMapNavControl(ctrl)
    ctrl._map.addControl @_navControl
    ctrl._map.addControl(new ZsMapAttribution(ctrl), 'bottom-left')
    return @
    
  enableModeControl: (enable = true)->
    @_modeControl.enable enable
    return @

  setMapMode: ()->
    @_modeControl.setMode()
    return @

  updateLanguage: ()->
    @_modeControl.updateLanguage()

class ZsMapControl
  constructor: (ctrl)->
    @_ctrl = ctrl
  onAdd: (map)->
    @_container = @addTo(map)
    @_container.className += ' mapboxgl-ctrl'
    @_container.addEventListener 'contextmenu', (e)->
      e.preventDefault()
    return @_container

  onRemove: ()->
    @_container.parentNode.removeChild(@_container)
    return @

class ZsMapNavControl extends ZsMapControl
  addTo: (map)->
    createButton = (className, parent, cb)->
      button = document.createElement('button')
      button.className = "mapboxgl-ctrl-icon mapboxgl-ctrl-zoom-#{className}"
      button.addEventListener 'click', cb
      parent.appendChild button
    
    container = document.createElement('div')
    container.className = 'mapboxgl-ctrl-group zsmap-ctrl-nav'
    createButton 'in', container, ()->
      map.zoomIn()
    createButton 'out', container, ()->
      map.zoomOut()
    
    return container
    
class ZsMapModeControl extends ZsMapControl
  _highlightClass: 'zsmap-ctrl-modeButton__highlight'
  
  constructor: (ctrl, options)->
    super(ctrl, options)
  
  addTo: (map)->
    @_container = document.createElement('div')
    @_container.className = 'mapboxgl-ctrl-group'

    createButton = (className, parent, cb)->
      button = document.createElement('div')
      button.className = "zsmap-ctrl-modeButton #{className}"
      button.addEventListener 'click', cb
      parent.appendChild button
      return button
      
    @_buttonLocation = createButton 'zsmapicon-cursor-pointer', @_container, ()=>
      @_ctrl.setConfig {topo: {mapSearch: false}}
    @_buttonMapSearch = createButton 'zsmapicon-map-marker-search', @_container, ()=>
      @_ctrl.setConfig {topo: {mapSearch: true}}
    @setMode()
    
    @_tooltip = document.createElement('div')
    @_tooltip.className = "zsmap-ctrl-tooltip hidden"
    @_tooltipText = document.createElement('span')
    @_tooltipText.innerHTML = @_ctrl._config.translations[@_ctrl._config.language].modeSwitch
    @_tooltip.appendChild(@_tooltipText)
    @_container.appendChild(@_tooltip)
    tooltipTimer = null
    @_container.addEventListener 'mouseenter', (e)=>
      tooltipTimer = setTimeout ()=>
        tooltipTimer = null
        @showTooltip true
      , 500
    @_container.addEventListener 'mouseleave', ()=>
      if tooltipTimer?
        clearTimeout tooltipTimer
        tooltipTimer = null
      @showTooltip false
    try
      unless localStorage?.getItem("zsMap.tooltips.mode") == 'true'
        @showTooltip true
        hideTip = ()=>
          map.off 'click', hideTip
          map.off 'move', hideTip
          @showTooltip false
        map.on 'click', hideTip
        map.on 'move', hideTip
        localStorage?.setItem("zsMap.tooltips.mode",'true')
    catch
      @showTooltip true
    
    return @_container
    
  enable: (enabled)->
    @_ctrl.utils.setClass @_container, 'hidden', !enabled
    return @
    
  setMode: ()->
    @_ctrl.utils.setClass @_buttonLocation, @_highlightClass, !@_ctrl._config.topo.mapSearch
    @_ctrl.utils.setClass @_buttonMapSearch, @_highlightClass, @_ctrl._config.topo.mapSearch
    return @
  
  showTooltip: (enabled = true)->
    @_ctrl.utils.setClass @_tooltip, 'hidden', !enabled
    return @
    
  updateLanguage: ()->
    @_tooltipText.innerHTML = @_ctrl._config.translations[@_ctrl._config.language].modeSwitch
    return @
       
class ZsMapAttribution extends ZsMapControl
  addTo: (map)->
    container = document.createElement('div')
    container.className = 'mapboxgl-ctrl-attrib'
    container.innerHTML = '<a href="https://www.mapbox.com/about/maps/" target="_blank">© Mapbox</a> <a href="http://www.openstreetmap.org/about/" target="_blank">© OpenStreetMap</a>'
    return container
    