ZsLeafletModeControl = L.Control.extend
  options: {position: 'topright'}
  _text:
    de: '<i class="zsicon2-cursor-pointer"></i>: Wähle Orte durch Klicken auf der Karte aus<br><i class="zsicon2-map-marker-search"></i>: Suche im Kartenausschnitt'
    en: '<i class="zsicon2-cursor-pointer"></i>: Choose locations by clicking on the map<br><i class="zsicon2-map-marker-search"></i>: Search in map section'

  initialize: (options)->
    L.Util.setOptions this, options
  
  onAdd: (map)->
    @_container = L.DomUtil.create 'div', 'leaflet-control leaflet-bar'
    @_buttonLoc = L.DomUtil.create 'div', 'zsmap-ctrl-modeButton zsmapicon-cursor-pointer', @_container
    @_buttonLoc.addEventListener 'click', ()=>
      @setMode false
      @options.onClick false
    @_buttonMap = L.DomUtil.create 'div', 'zsmap-ctrl-modeButton zsmapicon-map-marker-search', @_container
    @_buttonMap.addEventListener 'click', ()=>
      @setMode true
      @options.onClick true
    @setMode @options.mapSearch or false

    tooltip = L.DomUtil.create 'div', 'zsmap-ctrl-tooltip hidden', @_container
    @_tooltipText = L.DomUtil.create 'span', '', tooltip 
    @_tooltipText.innerHTML = @_text[@options.language]
    
    tooltipTimer = null
    @_container.addEventListener 'mouseenter', ()->
      tooltipTimer = setTimeout ()=>
        tooltipTimer = null
        L.DomUtil.removeClass tooltip, 'hidden'
      , 500
    @_container.addEventListener 'mouseleave', ()->
      if tooltipTimer?
        clearTimeout tooltipTimer
        tooltipTimer = null
      L.DomUtil.addClass tooltip, 'hidden'
    if @options.show?
      @show @options.show
    return @_container
    
  setMode: (val)->
    if val
      L.DomUtil.removeClass(@_buttonLoc, 'zsmap-ctrl-modeButton__highlight')
      L.DomUtil.addClass(@_buttonMap, 'zsmap-ctrl-modeButton__highlight')
    else
      L.DomUtil.addClass(@_buttonLoc, 'zsmap-ctrl-modeButton__highlight')
      L.DomUtil.removeClass(@_buttonMap, 'zsmap-ctrl-modeButton__highlight')
      
  show: (val)->
    if val
      L.DomUtil.removeClass(@_container, 'hidden')
    else
      L.DomUtil.addClass(@_container, 'hidden')
      
  updateLanguage: (lang)->
    @options.language = lang
    @_tooltipText.innerHTML = @_text[@options.language]
    
      
class ZsLeaflet
  _cfg:
    country: 'at'
    language: 'de'
    at:
      start: [[48.117666,16.18218],[48.322574,16.577511]]
      maxBounds: [[46.07, 9.23],[49.32, 17.46]]
    de:
      start: [[52.338079,13.088304],[52.675323,13.760909]]
      maxBounds: [[46.97, 5.56],[55.21, 15.34]]
  _events:
    clickOnEntity: []
    move: []
    load: []
    switchMapSearch: []
    estateDetail: []
  _selectedEntities: {}
  utils: new ZsMapUtils()
  _config: new ZsMapConfig()
  _mapSearch: false
  _search: true
  
  create: (mapId, options)->
    L.mapbox.accessToken = "pk.eyJ1Ijoiem9vbXNxdWFyZSIsImEiOiJiYTFhYmZlODkwNzQwZTc1OGQxMThjMTU1NzU1ZmM2ZSJ9.2eXIKouulITlZRWPCQRgkA"
    @_cfg.country = options.country
    @_cfg.language = options.language
    if options.topo?.search?
      @_search = options.topo.search
    if options.topo?.mapSearch?
      @_mapSearch = options.topo.mapSearch
    if options.topo?.entities?
      @setEntities options.topo.entities
    leafletOptions =
      minZoom: 7
      maxZoom: 18
      zoomControl: false
      attributionControl: false
      maxBounds: @_cfg[@_cfg.country].maxBounds
    @_map = L.mapbox.map(mapId, 'mapbox.streets', leafletOptions)
    @_modeControl = new ZsLeafletModeControl
      show: @_search
      mapSearch: @_mapSearch
      language: @_cfg.language
      onClick: (mapMode)=>
        if @_mapSearch != mapMode
          @_mapSearch = mapMode
          @_trigger 'switchMapSearch', mapMode
    @_map.addControl @_modeControl
    @_map.addControl L.control.zoom({position: 'topright'})
    @_map.addControl L.control.attribution({position: 'bottomleft', prefix: false}).addAttribution('<a href="https://www.mapbox.com/about/maps/" target="_blank">© Mapbox</a> <a href="https://.openstreetmap.org/about/" target="_blank">© OpenStreetMap</a>')
    @_map.addControl L.control.scale({position: 'bottomright', imperial: false})
    if options.bbox?
      @_map.fitBounds [[options.bbox[0][1],options.bbox[0][0]],[options.bbox[1][1],options.bbox[1][0]]]
    else
      @_map.fitBounds @_cfg[@_cfg.country].start
    @_map.on 'moveend', ()=>
      @_trigger 'move', @getBBox()
    @_trigger 'load', {fallback: true}
    return @

  setConfig: (options)->
    if options.language in ['de','en'] and @_cfg.language != options.language
      @_cfg.language = options.language
      @_modeControl.updateLanguage @_cfg.language
    if options.topo?.search? and @_search != options.topo.search
      @_search = options.topo.search
      @_modeControl.show @_search
    if options.topo?.mapSearch? and @_mapSearch != options.topo.mapSearch
      @_mapSearch = options.topo.mapSearch
      @_modeControl.setMode @_mapSearch

  getBBox: ()->
    b = @_map.getBounds().toBBoxString().split(',')
    return [[b[0],b[3]],[b[2],b[1]]]
  
  resize: ()->
    @_map.invalidateSize()

  panTo: (bbox, options)->
    @_map.fitBounds [[bbox[0][1],bbox[0][0]],[bbox[1][1],bbox[1][0]]]

  showGeo: (geo)->
    @hideGeo()
    @_geoArea = L.geoJson geo,
      style:
        color: '#00a3e0'
        weight: 2
        opacity: 1
        fillColor: '#00a3e0'
        fillOpacity: 0.6
    @_geoArea.addTo(@_map)
    @_geoMarker = L.marker @_convertPoint(@utils.getCenter(geo)), 
      icon: L.icon
        iconUrl: '/assets/shared/svg/map/zs-marker-active.svg'
        iconSize: [25,36]
        iconAnchor: [12,36]
    .addTo(@_map)

  hideGeo: ()->
    if @_geoArea?
      @_map.removeLayer @_geoArea
      @_geoArea = null
    if @_geoMarker?
      @_map.removeLayer @_geoMarker
      @_geoMarker = null

  showEstate: (geo)->
    if geo?
      @estateMode = true
      @showGeo geo
      @searchArea = {center: @_map.getCenter(),zoom: @_map.getZoom()}
      @panTo @utils.getBbox(geo)
    else
      @hideGeo()
      if @searchArea?
        @_map.setView @searchArea.center, @searchArea.zoom, {animate: true}
        @searchArea = null
      @estateMode = false

  showMarkers: (estates)->
    if @_markers?
      @_map.removeLayer @_markers
      @_markers = null
    if estates?.length > 0
      @_markers = L.markerClusterGroup
        showCoverageOnHover: false
        iconCreateFunction: (cluster)->
          count = cluster.getChildCount()
          if count >= 5
            if count >= 10
              size = 44
            else
              size = 38
          else
            size = 32
          L.divIcon
            iconSize: [size,size]
            className: 'zsCluster'
            html: "<div>#{count}</div>"
      estates.forEach (estate)=>
        center = @utils.getCenter(estate.geo.shape)
        marker = L.marker @_convertPoint(center),
          icon: L.icon
            iconUrl: '/assets/shared/svg/map/zs-marker.svg'
            iconSize: [25,36]
            iconAnchor: [12,36]
        marker.on 'click', (e)=>
          point = @_map.latLngToContainerPoint(e.latlng)
          container = @_map.getContainer()
          offset = [130,160]
          if point.x + 245 > container.clientWidth
            offset[0] = -130
          if point.y + 160 > container.clientHeight
            offset[1] = -20
          popup = L.popup
            className: 'zsmap-marker-estateList'
            autoPan: false
            closeButton: false
            offset: offset
            minWidth: 230
            maxWidth: 230
          html = document.createElement('div')
          text = "#{@utils.formatValue(estate.data?.space?.value)} m² / #{if estate.data?.rooms? then (estate.data?.rooms + " " + @_config.translations[@_cfg.language].rooms + " / ") else ""}#{@utils.formatValue(estate.data?.price?.value)} €"
          html.innerHTML = """
            <div class='zsmap-marker-estateList__img' style='background-image:url(#{estate.media?.images?[0] or ""})'></div>
            #{@_config.translations[@_cfg.language].types[estate.types.view] or estate.types.view}<br>
            <strong>#{text}</strong><br>
            <a class='button button--mini button--positive'>Details</a>"""
          html.addEventListener 'click', (e)=>
            e.stopPropagation()
            @_trigger 'estateDetail', estate.extId
          L.DomUtil.create('a','zsmap-marker-estateList__close zsicon2-close',html).addEventListener 'click', ()=> @_map.closePopup()
          popup.setContent(html).setLatLng(e.latlng).openOn(@_map)
        @_markers.addLayer marker
      @_map.addLayer @_markers
    
  remove: ()->
    for key of @_events
      @_events[key] = []
    @_map.remove()
    window.zsMap = new ZsLeaflet()

  getEntities: ()->
    Object.keys(@_selectedEntities)

  setEntities: (ids)->
    @_selectedEntities = {}
    for id in ids
      @_selectedEntities[id] = true

  addEntity: (id)->
    @_selectedEntities[id] = true

  removeEntity: (id)->
    delete @_selectedEntities[id]

  showBbox: (bbox)->
    if bbox?
      @_drawnBBox = L.polygon [[bbox[0],[bbox[0][0], bbox[1][1]],bbox[1],[bbox[1][0], bbox[0][1]],bbox[0]]],
        color: '#000000'
        weight: 2
      @_drawnBBox.addTo(@_map)
    else
      if @_drawnBBox?
        @_map.removeLayer @_drawnBBox
        @_drawnBBox = null

  _trigger: (event, payload)->
    if @_events[event]?
      for e in @_events[event]
        e.fn payload
    else
      throw new Error("zs-map._trigger: invalid event (#{event})")
      
  on: (event, tag, fn)->
    unless fn?
      fn = tag
      tag = null
    unless event?
      throw new Error("zs-map.on: 1st param missing (event)")
    unless fn?
      throw new Error("zs-map.on: 2nd param missing (function)")
    if @_events[event]?
      @_events[event].push
        tag: tag
        fn: fn
    else
      throw new Error("zs-map.on: invalid event (#{event})")
    return
    
  off: (tag)->
    unless tag?
      return 0
    count = 0
    for name, arr of @_events
      if arr.length > 0
        for index in [arr.length - 1..0]
          if arr[index].tag == tag
            count++
            arr.splice index, 1
    return count
    
  _convertPoint: (p)->
    return [p[1],p[0]]

email = 
  at: 'hello@zoomsquare.com'
  de: 'hallo@zoomsquare.de'
translations =
  de:
    bar: 'Einige Funktionen sind nicht verfügbar. <i class="zsicon2-alert-circle"></i>'
    modal: """
      Unsere Karte benötigt WebGL für den vollen Funktionsumfang.
      Du hast es möglicherweise deaktiviert oder dein Browser unterstützt WebGL nicht.<br><br>
      <a href="http://get.webgl.org/" target="_blank">Klicke hier</a>, um weiter Infos und Hilfe zum Aktivieren von WebGL zu erhalten.
      Bei weiteren Fragen wende dich gerne an unseren Support <a href='mailto:#email#'>#email#</a>."""
  en:
    bar: 'Some features are not available. <i class="zsicon2-alert-circle"></i>'
    modal: """Our map requires WebGL for full functionality.
      You may have disabled it, or your browser does not support WebGL.<br><br>
      <a href="http://get.webgl.org/" target="_blank">Click here</a> for more info and help to enable WebGL.
      You can also send us an email to <a href='mailto:#email#'>#email#</a>."""

if zsMap?.fallback?.id?
  options =
    country: zsMap._config.country
    language: zsMap._config.language
    topo:
      search: zsMap._config.topo.search
      mapSearch: zsMap._config.topo.mapSearch
  if zsMap.fallback.bbox?
    options.bbox = zsMap.fallback.bbox
  container = document.getElementById(zsMap.fallback.id)
  container.className += ' zsLeaflet-container'
  map = document.createElement('div')
  map.id = zsMap.fallback.id + '-map'
  map.className = 'zsLeaflet-map'
  container.appendChild(map)
  bar = document.createElement('div')
  bar.className = 'zsLeaflet-bar'
  bar.innerHTML = translations[options.language].bar
  container.appendChild(bar)
  modal = document.createElement('div')
  modal.className = 'zsLeaflet-modal hidden'
  modal.innerHTML = translations[options.language].modal.replace(/#email#/g,email[options.country])
  close = document.createElement('a')
  close.className = 'zsLeaflet-modal--close zsicon2-close'
  close.addEventListener 'click', ()->
    zsMap.utils.setClass modal, 'hidden', true
  modal.appendChild(close)
  container.appendChild(modal)
  bar.addEventListener 'click', ()->
    zsMap.utils.setClass modal, 'hidden', false
  events = zsMap._events
  afterLoadFns = zsMap._afterLoadFns
  window.zsMap = new ZsLeaflet()
  zsMap._events = events
  window.zsMap.create(map.id, options)
  for elem in afterLoadFns
    zsMap[elem.fn] elem.p1, elem.p2
