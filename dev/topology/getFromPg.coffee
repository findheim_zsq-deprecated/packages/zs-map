chalk = require 'chalk'
argv = require('optimist').argv
if argv.at
  country = "at"
else if argv.de
  country = "de"
else
  console.error chalk.red("Error") + ": No country given (use '--at' or '--de')"
  process.exit(1)

require("zs-pg").queryStreamToJSONFile
   sql: "select id, name, types, ST_asGeoJSON(geo) as geo, parents from loc_entity where types && ARRAY[4,6,8,9,10] and country_code='#{country}' and master is null and not hidden and not blacklisted"
   params: []
   lane: "bdm2"
   report: 10
   filename: "data-#{country}.json"  
 , (err) ->
   if err
     console.error err
   else
     console.log "done"
     
     
#"select l.id, types, ST_asGeoJSON(l.geo) as geo, ST_asGeoJSON(g.geo_100,6) as geo_100, nbefore, nafter from loc_entity l left join loc_entity_geo g on l.id=g.id where types && ARRAY[4,6,8,9,10] and country_code='at' and master is null"