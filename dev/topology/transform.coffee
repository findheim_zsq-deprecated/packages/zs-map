chalk = require 'chalk'
argv = require('optimist').argv
fs = require 'fs'

if argv.at
  country = "at"
else if argv.de
  country = "de"
else
  console.error chalk.red("Error") + ": No country given (use '--at' or '--de')"
  process.exit(1)

getCenter = (geo)->
  bbox = [[180,90],[0,0]]
  updateBBox = (point)->
    bbox[0][0] = Math.min(bbox[0][0], point[0])
    bbox[0][1] = Math.min(bbox[0][1], point[1])
    bbox[1][0] = Math.max(bbox[1][0], point[0])
    bbox[1][1] = Math.max(bbox[1][1], point[1])
  center = (bbox)->
    simplifyCoords [(bbox[0][0]+bbox[1][0])/2,(bbox[0][1]+bbox[1][1])/2]
  switch geo.type
    when 'Point' then geo.coordinates
    when 'Polygon'
      for ring in geo.coordinates
        for point in ring
          updateBBox(point)
      center bbox
    when 'MultiPolygon'
      for poly in geo.coordinates
        for ring in poly
          for point in ring
            updateBBox(point)
      center bbox
  
simplifyCoords = (data)->
  if typeof data == 'number'
    return Math.round(data * 10000) / 10000
  else
    for elem, index in data
      data[index] = simplifyCoords elem
    return data
    
simplifiyGeo = (geo)->
  geo.coordinates = simplifyCoords(geo.coordinates)
  return geo
  
data = {}
input = JSON.parse fs.readFileSync("data-#{country}.json","utf-8")
for entity in input
  data[entity.id] = entity
for id, entity of data
  for parent in entity.parents
    if data[parent]?
      data[parent].hasChildren = true

result =
  type: "FeatureCollection",
  features: []
  
for id, entity of data
  geo = JSON.parse(entity.geo)
  feature = 
    type: "Feature"
    geometry: simplifiyGeo(geo)
    properties:
      i: id
      n: entity.name
      c: getCenter(geo)
  for lvl in [4,6,8,9,10]
    maxLvl = Math.max.apply(null, entity.types)
    if entity.types.indexOf(lvl) >= 0
      feature.properties["#{lvl}"] = true
    if !entity.hasChildren && lvl > maxLvl && !(entity.id in ['10400000002495','10400000002498', '10400000002503']) #spread to higher levels (except 3 vienna districts)
      feature.properties["#{lvl}"] = true
    if '10400000002448' in entity.parents || '10400000002430' in entity.parents #salzburg and linz, spread level 10 to level 9
      feature.properties["9"] = true #children are on level 10, no level 9 exists, so show children also for level 9
  
  result.features.push feature
      
fs.writeFileSync("zs-topo-#{country}.json", JSON.stringify(result))

console.log "all done"