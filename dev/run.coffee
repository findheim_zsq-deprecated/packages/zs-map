webapp = require 'zs-webapp/lib/webapp'

app = webapp.express()

app.use webapp.static('public')

webapp.start()