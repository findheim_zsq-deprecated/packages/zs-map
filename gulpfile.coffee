gulp = require 'gulp'
gulpUtil = require 'gulp-util'
fs = require 'fs'
path = require 'path'
browserSync = require('browser-sync').create()

options =
  country: 'at'
  project: "zs-map"
  src: "src"
  dest:
    root: 'public'
    path: ''
  build:
    root: 'build'
  errorHandler: (label, err)->
    gulpUtil.log gulpUtil.colors.red("ERROR") + " - " + gulpUtil.colors.yellow("#{label}\n")
    gulpUtil.log err.toString()
    gulpUtil.log gulpUtil.colors.yellow("Waiting for file change\n")
  reload: ()->
    browserSync.reload()
  browserSync: browserSync

files = fs.readdirSync './gulp'
for file in files
  if path.extname(file) == '.coffee'
    require('./gulp/' + file)(options)

require('web-basic/shared/gulp')(gulp, options)
    
gulp.task "default", ["build-dev"], ->
  gulp.start "serve"