gulp = require "gulp"
$ = require('gulp-load-plugins')()
fs = require "fs"
translatorTypes = require('zs-translations').getTranslatorTypes()

module.exports = (options)->
  coffeeSrc = ["#{options.src}/scripts/**/*.coffee"]
  externalSourceFile = "#{options.src}/lib/external.json"
  devSrc = 'dev/dev.coffee'

  gulp.task "scripts-dev", ()->
    gulp.src devSrc
    .pipe $.plumber()
    .pipe $.coffee({bare: true}).on 'error', (err)-> options.errorHandler("Compile coffeescript", err)
    .pipe gulp.dest("#{options.dest.root}/assets/js")
  
  gulp.task "scripts", (done)->
    translatorTypes.loadFromServer()
    .then (translations)->
      externalSources = JSON.parse fs.readFileSync(externalSourceFile, "utf8")
      pipe = gulp.src coffeeSrc
      unless options.production
        pipe = pipe.pipe $.plumber()
      pipe = pipe.pipe $.change (content)->
        content = content.replace /"###zsTranslationTypesDe###"/g, JSON.stringify(translations.de)
        content = content.replace /"###zsTranslationTypesEn###"/g, JSON.stringify(translations.en)
        return content
      .pipe $.coffeelint()
      .pipe $.coffeelint.reporter()
      .pipe $.coffee({bare: true}).on 'error', (err)-> options.errorHandler("Compile coffeescript", err)
      .pipe $.addSrc.prepend externalSources
      .pipe $.concat('script.js')
      if options.production
        pipe = pipe.pipe $.uglify().on 'error', (err)-> options.errorHandler("Minify JS", err)
        .pipe gulp.dest("#{options.build.root}/")
      else
        pipe = pipe.pipe gulp.dest("#{options.dest.root}/assets/js")
      pipe.on 'end', done
    return

  gulp.task "scripts-dev-watch", ()->
    $.watch devSrc, ()->
      gulp.start 'scripts-dev'
      
  gulp.task "scripts-watch", ()->
    $.watch [externalSourceFile].concat(coffeeSrc), ()->
      gulp.start 'scripts'