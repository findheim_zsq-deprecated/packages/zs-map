gulp = require "gulp"
$ = require('gulp-load-plugins')()

module.exports = (options)->
  srcDevIndex = './dev/index.html'
  srcDevFavicon = './dev/favicon.ico'
  
  gulp.task "dev", ["dev-html","dev-favicon"]
    
  gulp.task "dev-html", ()->
    gulp.src srcDevIndex
    .pipe gulp.dest("#{options.dest.root}#{options.dest.path}")

  gulp.task "dev-favicon", ()->
    gulp.src srcDevFavicon
    .pipe gulp.dest("#{options.dest.root}#{options.dest.path}")

  gulp.task "dev-watch", ()->
    $.watch srcDevIndex, ()->
      $.util.log "Update '" + $.util.colors.cyan("dev") + "'"
      gulp.start 'dev'