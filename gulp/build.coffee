gulp = require "gulp"
$ = require('gulp-load-plugins')
  pattern: ['gulp-*', 'del', 'run-sequence']

module.exports = (options)->
  gulp.task "build-cleanup-dev", ()->
    $.del.sync "#{options.dest.root}"

  gulp.task "build-cleanup-prod", ()->
    $.del.sync "#{options.build.root}"

  gulp.task "build-dev", ["build-cleanup-dev"], (done)->
    $.runSequence ["dev", "scripts", "scripts-dev", "scripts-leaflet", "styles", "styles-leaflet", "assets-shared"], done
    
  gulp.task "build", ["build-cleanup-prod"], ()->
    options.production = true
    $.runSequence ["scripts", "scripts-leaflet", "styles", "styles-leaflet"], "build-prod-exit"

  gulp.task "build-prod-exit", ()->
    console.log "Process.exit - build complete"
    process.exit(0) #shitty mongoose prevents exit of gulp