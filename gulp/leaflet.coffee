gulp = require "gulp"
$ = require('gulp-load-plugins')()
fs = require "fs"

module.exports = (options)->
  coffeeSrc = ["#{options.src}/leaflet/**/*.coffee"]
  externalJS = "#{options.src}/lib/leaflet-external.json"
  srcSass = ["#{options.src}/style/leaflet.scss"]
  externalCSS = "#{options.src}/style/leaflet-external.json"

  gulp.task "scripts-leaflet", ()->
    externalSources = JSON.parse fs.readFileSync(externalJS, "utf8")
    pipe = gulp.src coffeeSrc
    unless options.production
      pipe = pipe.pipe $.plumber()
    pipe = pipe.pipe $.coffeelint()
    .pipe $.coffeelint.reporter()
    .pipe $.coffee().on 'error', (err)-> options.errorHandler("Compile coffeescript", err)
    .pipe $.addSrc.prepend externalSources
    .pipe $.concat('leaflet.js')
    if options.production
      pipe = pipe.pipe $.uglify().on 'error', (err)-> options.errorHandler("Minify JS", err)
      .pipe gulp.dest("#{options.build.root}/")
    else
      pipe = pipe.pipe gulp.dest("#{options.dest.root}/assets/shared/js")

  gulp.task "scripts-leaflet-watch", ()->
    $.watch [externalJS].concat(coffeeSrc), ()->
      gulp.start 'scripts-leaflet'

  gulp.task "styles-leaflet", ->
    externalSources = JSON.parse fs.readFileSync(externalCSS, "utf8")
    pipe = gulp.src srcSass
    unless options.production
      pipe = pipe.pipe $.plumber()
    pipe = pipe.pipe $.sass({errLogToConsole: true,outputStyle: 'expanded'}).on 'error', (err)-> options.errorHandler("Compile sass", err)
    .pipe $.addSrc.prepend externalSources
    .pipe $.autoprefixer({browsers: ['last 2 versions']}).on 'error', (err)-> options.errorHandler("Autoprefix sass", err)
    .pipe $.concat('leaflet.css')
    if options.production
      pipe.pipe $.minifyCss().on 'error', (err)-> options.errorHandler("Minify css", err)
      .pipe gulp.dest("#{options.build.root}")
    else
      pipe.pipe gulp.dest("#{options.dest.root}/assets/shared/css/")

  gulp.task "styles-leaflet-watch", ()->
    $.watch [externalCSS].concat(srcSass), ()->
      gulp.start "styles-leaflet"