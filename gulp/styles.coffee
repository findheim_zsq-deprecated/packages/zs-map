gulp = require "gulp"
$ = require('gulp-load-plugins')()
fs = require "fs"

module.exports = (options)->
  srcSass = ["#{options.src}/style/screen.scss"]
  srcFramework = ["dev/style/screen.scss"]
  watchSass = ["#{options.src}/style/**/*.scss"]
  externalSourceFile = "#{options.src}/style/external.json"

  gulp.task "styles", ->
    externalSources = JSON.parse fs.readFileSync(externalSourceFile, "utf8")
    pipe = gulp.src srcSass
    unless options.production
      pipe = pipe.pipe $.plumber()
      .pipe $.addSrc.prepend srcFramework
    pipe = pipe.pipe $.sass({errLogToConsole: true,outputStyle: 'expanded'}).on 'error', (err)-> options.errorHandler("Compile sass", err)
    .pipe $.addSrc.prepend externalSources
    .pipe $.autoprefixer({browsers: ['last 2 versions']}).on 'error', (err)-> options.errorHandler("Autoprefix sass", err)
    .pipe $.concat('style.css')
    if options.production
      pipe.pipe $.minifyCss().on 'error', (err)-> options.errorHandler("Minify css", err)
      .pipe gulp.dest("#{options.build.root}")
    else
      pipe.pipe gulp.dest("#{options.dest.root}/assets/css/")

  gulp.task "styles-watch", ()->
    $.watch [externalSourceFile].concat(watchSass), ()->
      gulp.start "styles"