gulp = require 'gulp'
$ = require('gulp-load-plugins')()

cfg = require('zs-app/lib/config')

browserSyncCfg = cfg.get("browserSync")

module.exports = (options)->
  gulp.task "serve", ["nodemon"], ->
    gulp.start "watch"
    options.browserSync.init
      proxy: "http://localhost:#{cfg.get("port")}",
      port: browserSyncCfg.port
      files: ["#{options.dest.root}/**/*.*"]
      online: false
      notify: false
      ui: {port: browserSyncCfg.uiPort}
      reloadDelay: browserSyncCfg.reloadDelay

  nodemonStarted = false
  nodemonRestart = false
  gulp.task "nodemon", (cb)->
    $.nodemon
      script: "dev/run.coffee"
      ext: 'coffee html css json'
      ignore: []
      watch: ["config","dev","public"]
      stdout: false
    .on 'stdout', (msg)-> #manual process to detect when the webserver is available... only then finish task
      msg = msg.toString()
      process.stdout.write msg
      if msg.indexOf("Web server listening") >= 0
        if !nodemonStarted
          nodemonStarted = true
          cb()
        if nodemonRestart
          nodemonRestart = false
          options.browserSync.reload()
    .on 'stderr', (msg)->
      process.stderr.write msg.toString()
    .on 'restart', ()->
      nodemonRestart = true
  
  gulp.task "watch", ["dev-watch", "scripts-watch", "scripts-dev-watch", "scripts-leaflet-watch", "styles-watch", "styles-leaflet-watch"]