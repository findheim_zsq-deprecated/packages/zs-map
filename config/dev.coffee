module.exports = 
  port: 2400
  browserSync:
    port: 2401
    uiPort: 2402
    reloadDelay: 200
  disableExpressLogging: true